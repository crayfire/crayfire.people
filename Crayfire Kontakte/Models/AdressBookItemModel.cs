﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crayfire_Kontakte
{
    public class AdressBookItemModel : INotifyPropertyChanged
    {
        private string _DisplayName = string.Empty;
        public string DisplayName
        {
            get
            {
                return this._DisplayName;
            }

            set
            {
                if (this._DisplayName != value)
                {
                    this._DisplayName = value;
                    this.OnPropertyChanged("DisplayName");
                }
            }
        }

        private string _DisplayEmail = string.Empty;
        public string DisplayEmail
        {
            get
            {
                return this._DisplayEmail;
            }

            set
            {
                if (this._DisplayEmail != value)
                {
                    this._DisplayEmail = value;
                    this.OnPropertyChanged("DisplayEmail");
                }
            }
        }

        private string _DisplayPhone = string.Empty;
        public string DisplayPhone
        {
            get
            {
                return this._DisplayPhone;
            }

            set
            {
                if (this._DisplayPhone != value)
                {
                    this._DisplayPhone = value;
                    this.OnPropertyChanged("DisplayPhone");
                }
            }
        }

        private string _FullAdress = string.Empty;
        public string FullAdress
        {
            get
            {
                return this._FullAdress;
            }

            set
            {
                if (this._FullAdress != value)
                {
                    this._FullAdress = value;
                    this.OnPropertyChanged("FullAdress");
                }
            }
        }

        private string _FullName = string.Empty;
        public string FullName
        {
            get
            {
                return this._FullName;
            }

            set
            {
                if (this._FullName != value)
                {
                    this._FullName = value;
                    this.OnPropertyChanged("FullName");
                }
            }
        }

        private string _FirstName = string.Empty;
        public string FirstName
        {
            get
            {
                return this._FirstName;
            }

            set
            {
                if (this._FirstName != value)
                {
                    this._FirstName = value;
                    this.OnPropertyChanged("FirstName");
                }
            }
        }

        private string _Name = string.Empty;
        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        private string _Addition = String.Empty;
        public string Addition
        {
            get
            {
                return this._Addition;
            }

            set
            {
                if (this._Addition != value)
                {
                    this._Addition = value;
                    this.OnPropertyChanged("Addition");
                }
            }
        }

        private string _FullStreet = string.Empty;
        public string FullStreet
        {
            get
            {
                return this._FullStreet;
            }

            set
            {
                if (this._FullStreet != value)
                {
                    this._FullStreet = value;
                    this.OnPropertyChanged("FullStreet");
                }
            }
        }

        private string _StreetName = string.Empty;
        public string StreetName
        {
            get
            {
                return this._StreetName;
            }

            set
            {
                if (this._StreetName != value)
                {
                    this._StreetName = value;
                    this.OnPropertyChanged("StreetName");
                }
            }
        }

        private string _StreetNumber = string.Empty;
        public string StreetNumber
        {
            get
            {
                return this._StreetNumber;
            }

            set
            {
                if (this._StreetNumber != value)
                {
                    this._StreetNumber = value;
                    this.OnPropertyChanged("StreetNumber");
                }
            }
        }

        private string _ZipCode = string.Empty;
        public string ZipCode
        {
            get
            {
                return this._ZipCode;
            }

            set
            {
                if (this._ZipCode != value)
                {
                    this._ZipCode = value;
                    this.OnPropertyChanged("ZipCode");
                }
            }
        }

        private string _City = string.Empty;
        public string City
        {
            get
            {
                return this._City;
            }

            set
            {
                if (this._City != value)
                {
                    this._City = value;
                    this.OnPropertyChanged("City");
                }
            }
        }

        private string _Phone = string.Empty;
        public string Phone
        {
            get
            {
                return this._Phone;
            }

            set
            {
                if (this._Phone != value)
                {
                    this._Phone = value;
                    this.OnPropertyChanged("Phone");
                }
            }
        }

        private string _Fax = string.Empty;
        public string Fax
        {
            get
            {
                return this._Fax;
            }

            set
            {
                if (this._Fax != value)
                {
                    this._Fax = value;
                    this.OnPropertyChanged("Fax");
                }
            }
        }

        private string _Mobile = string.Empty;
        public string Mobile
        {
            get
            {
                return this._Mobile;
            }

            set
            {
                if (this._Mobile != value)
                {
                    this._Mobile = value;
                    this.OnPropertyChanged("Mobile");
                }
            }
        }

        private string _Email = string.Empty;
        public string Email
        {
            get
            {
                return this._Email;
            }

            set
            {
                if (this._Email != value)
                {
                    this._Email = value;
                    this.OnPropertyChanged("Email");
                }
            }
        }

        private string _Website = string.Empty;
        public string Website
        {
            get
            {
                return this._Website;
            }

            set
            {
                if (this._Website != value)
                {
                    this._Website = value;
                    this.OnPropertyChanged("Website");
                }
            }
        }

        private int _AdressBookID;
        public int AdressBookID
        {
            get
            {
                return this._AdressBookID;
            }

            set
            {
                if (this._AdressBookID != value)
                {
                    this._AdressBookID = value;
                    this.OnPropertyChanged("AdressBookID");
                }
            }
        }
            

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}

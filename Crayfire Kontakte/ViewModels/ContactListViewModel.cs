﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

using Telerik.Windows.Controls;
using Telerik.Windows.Controls.FileDialogs;
using Telerik.Windows.Data;

using Crayfire.Components.vCard;
using System.IO;

namespace Crayfire_Kontakte
{

    public class ContactListViewModel : ViewModelBase
    {
        ICollectionView _itemsSource;
        public DelegateCommand CreateVcardCommand { get ; private set; }
        public DelegateCommand ContactItemAddCommand { get; private set; }
        public DelegateCommand ContactEditCommand { get; private set; }
        public DelegateCommand ContactAddCommand { get; private set; }
        public DelegateCommand ContactDeleteCommand { get; private set; }

        

        public ContactListViewModel()
        {
            this.CreateVcardCommand = new DelegateCommand(CreateVcardAction);
            this.ContactItemAddCommand = new DelegateCommand(ContactItemAddAction);
            this.ContactAddCommand = new DelegateCommand(ContactAddAction);
            this.ContactEditCommand = new DelegateCommand(ContactEditAction);
            this.ContactDeleteCommand = new DelegateCommand(ContactDeleteAction);
        }
        public void CreateVcardAction(object obj)
        {
            AdressBookItemModel ccc = ItemsSource.CurrentItem as AdressBookItemModel;
            vCardHandler vCard = new vCardHandler();
            FileStream fileStream = null;
            vCard.FirstName = ccc.FirstName;
            vCard.LastName = (ccc.Name + " " + ccc.Addition).Trim();
            vCard.AddAddress((ccc.StreetName+ " " +ccc.StreetNumber).Trim(), ccc.ZipCode, ccc.City,"",AddressType.HOME);
            vCard.AddMailAddress(ccc.Email, EmailType.HOME);
            vCard.AddPhoneNumber(ccc.Phone, TelephoneNumberType.HOME);
            vCard.AddPhoneNumber(ccc.Mobile, TelephoneNumberType.CELL);
            vCard.AddPhoneNumber(ccc.Fax,TelephoneNumberType.FAX);
            RadSaveFileDialog SaveFileDialog = new RadSaveFileDialog();
            SaveFileDialog.ExpandToCurrentDirectory = false;

            SaveFileDialog.InitialDirectory = @"C:\";
            SaveFileDialog.FileName = (ccc.FirstName + " " + ccc.Name + " " + ccc.Addition).Trim();
            SaveFileDialog.Filter =
                "|vCard File|*.vcf" +
                "|All Files|*.*";
            SaveFileDialog.FilterIndex = 1;
            SaveFileDialog.ShowDialog();
            if (SaveFileDialog.DialogResult == true)
            {
                string filePath = SaveFileDialog.FileName;
                fileStream = new FileStream(filePath,FileMode.Create, FileAccess.Write);
                fileStream.Close();
                using (StreamWriter writer = new StreamWriter(filePath, true, new UTF8Encoding(false)))
                {
                    try
                    {
                        string temp = vCard.vCardContent();
                        writer.Write(temp);


                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            

        }
        public void ContactAddAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new ContactAddViewModel();
            rw.Content = new ContactAddView();
            rw.Name = "ContactAddWindow";
            rw.Width = 550;
            rw.MaxWidth = 550;
            rw.MinWidth = 550;
            rw.Height = 850;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Kontakt erstellen";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }
        public void ContactEditAction(object obj)
        {
            AdressBookItemModel ccc = ItemsSource.CurrentItem as AdressBookItemModel;
            RadWindow rw = new RadWindow();
            rw.DataContext = new ContactAddViewModel(ccc.AdressBookID);
            rw.Content = new ContactAddView();
            rw.Width = 550;
            rw.MaxWidth = 550;
            rw.MinWidth = 550;
            rw.Height = 850;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Kontakt bearbeiten";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }
        public void ContactItemAddAction(object obj)
        {
            RadWindow rw = new RadWindow();
            rw.DataContext = new ContactAddViewModel(1);
            rw.Content = new ContactAddView();
            rw.Width = 550;
            rw.MaxWidth = 550;
            rw.MinWidth = 550;
            rw.Height = 800;
            rw.Owner = App.Current.MainWindow;
            rw.Header = "Neuer Kontakt";
            rw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            rw.ShowDialog();
        }
        public void ContactDeleteAction(object obj)
        {
            RadWindow.Confirm("Sind sie sicher den Datensatz löschen zu wollen?", this.OnClosed);            
        }

        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            AdressBookItemModel ccc = ItemsSource.CurrentItem as AdressBookItemModel;
            CetusSQLiteConnection DB = new CetusSQLiteConnection();
            var result = e.DialogResult;
            if (result == true)
            {
                DB.DeleteAdressBookItem(ccc.AdressBookID);
                //Console.WriteLine(ccc.AdressBookID.ToString() + " wurde gelöscht");
            }
        }



        public ICollectionView ItemsSource
        {
            get
            {
                if (_itemsSource == null)
                {
                    CetusSQLiteConnection DB = new CetusSQLiteConnection();
                    CollectionViewSource collectionViewSource = new CollectionViewSource();
                    collectionViewSource.Source = DB.SelectAdressBook();
                    _itemsSource = collectionViewSource.View;
                    _itemsSource.MoveCurrentToFirst();

                }

                return _itemsSource;
            }
        }


    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;

namespace Crayfire_Kontakte
{
    public class ContactAddViewModel : ViewModelBase
    {

        public int ContactID = 0;
        public AdressBookItemModel _ContactItem;

        public Image ProfilePicture { get;  set; }

        public Action CloseWindow { get; set; }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand AbortCommand { get; private set; }
        public DelegateCommand ImageCropp { get; private set; }

        public DelegateCommand UploadPictureCommand { get; private set; }

        public AdressBookItemModel ContactItem
        {
            get
            {
                if (this.ContactID != 0 && this._ContactItem == null)
                {
                CetusSQLiteConnection DB = new CetusSQLiteConnection();
                _ContactItem = DB.SelectAdressBookItem(this.ContactID);
                return _ContactItem;
                }
                //if (ContactID == 0)
                //{
                //    return _ContactItem;
                //}
                return _ContactItem;


            }
            set
            {
                _ContactItem = value;
            }
        }

        public object DataContext { get; private set; }

        public ContactAddViewModel(int id = 0)
        {
            this.SaveCommand = new DelegateCommand(SaveAction);
            this.AbortCommand = new DelegateCommand(AbortAction);
            ImageCropp = new DelegateCommand(ImageCroppAction);
            UploadPictureCommand = new DelegateCommand(UploadPictureAction);

            if (id != 0)
            {
                this.ContactID = id;

            }
            if(id == 0)
            {
                this.ContactID = 0;
                this.ContactItem = new AdressBookItemModel();
            }

        }

        public void SaveAction(object obj)
        {
            CetusSQLiteConnection db = new CetusSQLiteConnection();
            if (this.ContactID == 0)
            {
                if (this.ContactItem.Name != String.Empty)
                {
                    db.InsertAdressBookItem(this.ContactItem);
                }
                
            }
            if (this.ContactID > 0)
            {
                if (this.ContactItem.Name != String.Empty)
                {
                    db.UpdateAdressBookItem(this.ContactItem);
                }
            }
            Window CurrentModalWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            CurrentModalWindow.Close();
            CurrentModalWindow = null;

        }

        public void AbortAction(object obj)
        {
            // see here for more details https://stackoverflow.com/questions/2038879/refer-to-active-window-in-wpf

            Window CurrentModalWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            CurrentModalWindow.Close();
            CurrentModalWindow = null;
        }

        public void ImageCroppAction(object obj)
        {
            Console.WriteLine("Image Crop!");
        }

        public void UploadPictureAction(object obj)
        {
            RadOpenFileDialog openFileDialog = new RadOpenFileDialog();
            openFileDialog.ExpandToCurrentDirectory = false;
            openFileDialog.InitialDirectory = @"%USERPROFILE";

            openFileDialog.ShowDialog();

            if (openFileDialog.DialogResult == true)
            {
                string fileName = openFileDialog.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(fileName, UriKind.Relative);
                bitmap.EndInit();
                // ProfilePicture = System.Drawing.Image.FromFile(fileName);
                ProfilePicture.Source = new BitmapImage(new Uri (@"C:\CrayfireCases\1810001\1810001 Fahrzeugschein hinten.jpg"));
            }
        }

    }
}

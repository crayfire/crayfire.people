﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crayfire_Kontakte
{
    public class CetusSQLiteConnection
    {
        private string dataSource = "cetus.db";
        private SQLiteConnection connection;
        private SQLiteCommand command;

        public CetusSQLiteConnection() { }

        private void Open()
        {
            connection = new SQLiteConnection();
            connection.ConnectionString = "Data Source=" + dataSource;
            connection.Open();
        }

        private void Close()
        {
            connection.Close();
            connection.Dispose();
        }

        public bool InsertAdressBookItem(AdressBookItemModel item)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "INSERT INTO adressbook(firstname,name,addition,streetname,streetnumber,zipcode,city,phone,fax,mobile,email,website,note,adresstype) " +
                "VALUES ('"+item.FirstName+"','"+item.Name+ "','" + item.Addition + "','" + item.StreetName + "','" + item.StreetNumber + "','" + item.ZipCode + "','" + item.City + "','" + item.Phone + "','" + item.Fax + "','" + item.Mobile + "','" + item.Email + "','" + item.Website + "',NULL,NULL)";
            command.ExecuteNonQuery();
            Close();
            return true;
        }
        public bool UpdateAdressBookItem(AdressBookItemModel item)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "UPDATE adressbook SET " +
                "firstname='" + item.FirstName.Trim() + "'," +
                "name='" + item.Name.Trim() + "'," +
                "addition='" + item.Addition + "'," +
                "streetname='" + item.StreetName + "'," +
                "streetnumber='" + item.StreetNumber + "'," +
                "zipcode='" + item.ZipCode + "'," +
                "city='" + item.City + "'," +
                "phone='" + item.Phone + "'," +
                "fax='" + item.Fax + "'," +
                "mobile='" + item.Mobile + "'," +
                "email='" + item.Email + "'," +
                "website='" + item.Website + "' " +
                "WHERE adressbookID='" + item.AdressBookID + "'";
            command.ExecuteNonQuery();
            Close();
            return true;
        }

        public SQLiteDataReader Select(String text)
        {
            Open();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            command.CommandText = text;
            SQLiteDataReader reader = command.ExecuteReader();
            Close();
            
            return reader;
        }



        public ObservableCollection<AdressBookItemModel> SelectAdressBook(string adresstype = null)
        {
            ObservableCollection<AdressBookItemModel> AdressBook = new ObservableCollection<AdressBookItemModel>();
            Open();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            if (adresstype == null)
            {
                //command.CommandText = "SELECT * FROM adressbook";
                command.CommandText = "SELECT firstname, name, addition, streetname, streetnumber, zipcode, city, phone, fax, email, mobile, adressbookID FROM adressbook";

            }
            else
            {
                command.CommandText = "SELECT * FROM adressbook WHERE adresstype='" + adresstype + "'";
            }

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                AdressBook.Add(new AdressBookItemModel
                {
                    FullName = (reader["firstname"].ToString() + " " + reader["name"].ToString() + " " + reader["addition"].ToString()).Trim(),
                    FirstName = reader["firstname"].ToString(),
                    Name = reader["name"].ToString(),
                    Addition = reader["addition"].ToString(),
                    FullAdress = reader["streetname"].ToString() + " " + reader["streetnumber"].ToString() + " " + reader["zipcode"].ToString() + " " + reader["city"].ToString(),
                    StreetName = reader["streetname"].ToString(),
                    StreetNumber = reader["streetnumber"].ToString(),
                    ZipCode = reader["zipcode"].ToString(),
                    City = reader["city"].ToString(),
                    Phone = reader["phone"].ToString(),
                    Fax = reader["fax"].ToString(),
                    Mobile = reader["mobile"].ToString(),
                    Email = reader["email"].ToString(),
                    //Website = reader[11].ToString()
                    AdressBookID = Int16.Parse(reader["adressbookID"].ToString()),
                });
            }
            Close();
            return AdressBook;
        }
        //SELECT r.reportID, r.reportName, r.reportDate, r.status, c.firstname, c.lastname, v.licensePlate, v.vin, v.manufacturer, v.model  FROM reports r, contacts c, vehicles v WHERE c.contactID = r.reportID AND c.type="AS" AND v.vehicleID = r.reportID
        public AdressBookItemModel SelectAdressBookItem(int ContactTD)
        {
            Open();
            AdressBookItemModel AdressBook = new AdressBookItemModel();
            command = new SQLiteCommand(connection);
            // Auslesen des zuletzt eingefügten Datensatzes.
            command.CommandText = "SELECT firstname, name,addition, streetname, streetnumber, zipcode, city, phone, fax, email, mobile,adressbookID FROM adressbook WHERE adressbookID='" + ContactTD + "' LIMIT 1";

            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                AdressBook =new AdressBookItemModel
                {
                    FullName = reader["firstname"].ToString() + " " + reader["name"].ToString(),
                    FirstName = reader["firstname"].ToString(),
                    Name = reader["name"].ToString(),
                    Addition = reader["addition"].ToString(),
                    FullAdress = reader["streetname"].ToString() + " " + reader["streetnumber"].ToString() + " " + reader["zipcode"].ToString() + " " + reader["city"].ToString(),
                    StreetName = reader["streetname"].ToString(),
                    StreetNumber = reader["streetnumber"].ToString(),
                    ZipCode = reader["zipcode"].ToString(),
                    City = reader["city"].ToString(),
                    Phone = reader["phone"].ToString(),
                    Fax = reader["fax"].ToString(),
                    Mobile = reader["mobile"].ToString(),
                    Email = reader["email"].ToString(),
                    //Website = reader[11].ToString()
                    AdressBookID = Int16.Parse( reader["adressbookID"].ToString()),
                };

            }
            return AdressBook;
        }

        public bool DeleteAdressBookItem(int AddressID)
        {
            Open();
            command = new SQLiteCommand(connection);
            command.CommandText = "DELETE FROM adressbook WHERE adressbookID ='" + AddressID + "'";
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
                return false;
                //throw;
            }
        }
    }
}
